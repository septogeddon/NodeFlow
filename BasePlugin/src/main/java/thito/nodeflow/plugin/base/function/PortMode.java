package thito.nodeflow.plugin.base.function;

public enum PortMode {
    MULTIPLE, SINGLE, NONE;
}
