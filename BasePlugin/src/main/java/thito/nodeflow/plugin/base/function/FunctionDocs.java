package thito.nodeflow.plugin.base.function;

import javafx.scene.Node;

public interface FunctionDocs {
    Node createNode();
}
